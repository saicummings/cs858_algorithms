/**
 * trie.h
 * sai lekyang
 */

#if !defined(_TRIE_H_)
#define _TRIE_H_

#define ASIZE 52

/* A linked list node. */
struct trie_node {
	struct trie_node *children[ASIZE];
	int end;
};

int get_index(char c);

char get_char(unsigned int i);

struct trie_node* make_trie_node();

int trie_insert(struct trie_node *node, char *w);

int trie_search(struct trie_node *root, char *w);

void free_trie(struct trie_node *head);

/*
unsigned int trie_suggestion(char *word,FILE *outfile, 
			unsigned int edits, struct trie_node * root, int add_delete);
*/
#endif /* !_TRIE_H_ */
