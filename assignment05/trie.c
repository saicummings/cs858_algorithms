/**
 * 
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "trie.h"

int get_index(char c){
	if (c < 'a'){
		return c - 'A';
	} else {
		return c - 'a' + 26;
	}
}

char get_char(unsigned int i){
	if (i >= 26){
		return i + 'a' - 26;
	} else {
		return i + 'A';
	}

}

struct trie_node* make_trie_node(){
	int i;
	struct trie_node *new_node;
	new_node = calloc(1, sizeof(*new_node));
	if (!new_node) {
		perror("calloc failed");
	}

	for (i = 0; i < ASIZE; i++){
		new_node->children[i] = NULL;
	}

	return new_node;
}


int trie_insert(struct trie_node *root, char *w)
{
	int wlen, i;
	struct trie_node *node;
	node = root;
	assert(node);
	wlen = strlen(w);
	/* loop through trie tree, add node if does not exist */
	for (i = 0; i < wlen; i++){
		int indx = get_index(w[i]);

		if(!node->children[indx]){
			/* node nil, make newchild */
			node->children[indx] = make_trie_node();
		}
		/* node not nil, move to child */
		node = node->children[indx];

	}

	node->end = 1;
	return 0;
}

/* return 1 for true, 0 for false */
int trie_search(struct trie_node *root, char *w){
	int wlen, i;
	struct trie_node *node = root;

	wlen = strlen(w);

	for (i = 0; i < wlen; i++){
		int indx;
		indx = get_index(w[i]);
		if(node->children[indx] == NULL){
			return 0;
		} else {
			node = node->children[indx];
		}
	}

	if(!node || node->end == 0)
		return 0;
	else
		return 1;
}

void free_trie(struct trie_node *head)
{
	if (head) {
		int i;
		struct trie_node * node;
		node = head;
		for (i = 0; i < ASIZE; i++){
			if (node->children[i])
				free_trie(node->children[i]);
		}

		free(head);
	}
}




