First version
Initial Release

Second Version
Thursday, September 13, 2012 at 3:41 PM

Changed char* to be unsigned char* so that they add together
properly.
Modified makefile to us the proper flags
Changed files so that makefile's flags still compiled

Third Version
Saturday, September 15, 2012 at 12:45 PM

Fixed compilation error that prevented compilation on agate (but now
it won't compile on a 64 bit machine).

Fourth Version
Saturday, September 15 at 3:10 PM

Fixed makefile
