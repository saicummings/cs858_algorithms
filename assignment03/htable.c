/*
 * Code file for hash tables.
 *
 * Saisuk Lekyang
 * 
 * Assignment 3
 * This is primarily where you will need to add things.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "storer.h"
#include "htable.h"
#include "eprintf.h"

static const int NHASH = 5000;

static State* ht_lookup(unsigned char* prefix[], int create, PrefixStorer* ps);
static void ht_add(unsigned char* prefix[], unsigned char* word, PrefixStorer* ps);
static void cleanup(PrefixStorer* ps);
static void printCounts(PrefixStorer* ps);
unsigned int badHash(unsigned char* s[NPREF]);
unsigned int krHash(unsigned char* s[NPREF]);
unsigned int add3Hash(unsigned char* s[NPREF]);
unsigned int cwHash(unsigned char* s[NPREF]);

static unsigned int * table256;
static unsigned int table_size = NHASH;
static unsigned int entry_size = 0;
static const unsigned int load_factor = 1;

static const int EXPNDFCT = 2;


/*
 * Create a new hash table and returns a pointer to the created hash table.
 */
PrefixStorer* makeHashPrefixStorer(enum hashType ht)
{
  int i;
  State ** hashtable;

  table256 = emalloc(sizeof(unsigned int) * 256);

  for (i = 0; i < 256; i++){
    table256[i] = rand();
  }


  PrefixStorer* p;
  /*
   * TODO:
   * You will have to decide exactly what you want to malloc here, as
   * it will depend on how you decide to implement your hash table.
   *
   * PrefixStorer* p = emalloc(?);
   */

  p = emalloc(sizeof(PrefixStorer));

  hashtable = emalloc(NHASH * sizeof(State*));

  p->tableBase = hashtable;

  for (i = 0; i < NHASH; i++)
    hashtable[i] = NULL;

  p->lookup = &ht_lookup;
  p->add = &ht_add;
  p->cleanup = &cleanup;
  p->printCounts = &printCounts;

  switch (ht) {
    case BAD:
      p->hash = &badHash;
      break;
    case KR:
      p->hash = &krHash;
      break;
    case CW:
      p->hash = &cwHash;
      break;
    case ADD3:
      p->hash = &add3Hash;
      break;
  }

  return p;
}

/*
 * You might find this function useful, it tells how long a bucket is.
 */
/*
static unsigned int length(State* s)
{
  unsigned int l = 0;
  while (s != NULL) {
    s = s->next;
    l++;
  }
  return l;
}
*/

static void printCounts(PrefixStorer* ps)
{
  /*
    You will need to make sure this is implemented to work with your
    hash table.  Once again, the precise mechanics of this function
    will depend on how you decide to implement your hash table.

  for(every bucket in your table){
  printf("bucket %i:\t%i\n", i, length(statetab[i]));
  }
  */

  int i = 0;
  int len = 0;
  State** statehead = (State**) (ps->tableBase);
  
  State* sp;

  for (i = 0; i < table_size; i++) {
    sp = statehead[i];

    while (sp != NULL){
      ++len;
      sp = sp->next;
    }
    printf("bucket %i:\t%i\n", i, len);
    
    len = 0;
  }
}

/*
 * Delete all nodes in your hash table.
 */
static void cleanup(PrefixStorer* ps)
{
  int i = 0;
  State** statehead = (State**) (ps->tableBase);
  
  State* sp;
  
  for (i = 0; i < table_size; i++) {
    sp = statehead[i];
      while (sp != NULL){
      State* current = sp;
      sp = sp->next;
      cleanupState(current);
      free(current);
    }
  }
  free(statehead);
  free(table256);

}

/*
 * Takes an array of strings and flattens it, so a string hash
 * function can be used on it.  It does so by just concatenating the
 * strings together.
 */
static unsigned char* flatten(unsigned char* s[NPREF])
{
  int size = 0;
  unsigned char* longString;
  int i;
  for (i = 0; i < NPREF; i++) {
    size += strlen((char*) s[i]);
  }
  longString = emalloc(size + 1);
  size = 0;
  for (i = 0; i < NPREF; i++) {
    strcpy((char*) longString + size, (char*) s[i]);
    size += strlen((char*) s[i]);
  }
  return longString;

}
/* hash: compute hash value for array of NPREF strings */
unsigned int badHash(unsigned char* s[NPREF])
{
  unsigned char* flat = flatten(s);
  unsigned int h;
  h = flat[0];
  free(flat);
  return h % table_size;
}

/* hash: compute hash value for array of NPREF strings */
unsigned int krHash(unsigned char* s[NPREF])
{
  unsigned char* toHash = flatten(s);
  unsigned char* base = toHash;
  unsigned int hash = 0;
  unsigned int mult = 127;

  /*printf("%ld\n", strlen((char *)toHash) );*/


  while (*toHash != '\0'){
    hash = hash * mult + *toHash;
    ++toHash;
  }


  free(base);
  return hash % table_size;
}

/* hash: compute hash value for array of NPREF strings */
unsigned int cwHash(unsigned char* s[NPREF])
{
  unsigned char* toHash = flatten(s);
  unsigned char* base = toHash;
  unsigned int hash = 0;

  /*printf("%ld\n", strlen((char *)toHash) );*/

  while (*toHash != '\0'){
    /* rotate the bits in hash by 1 */
    hash = hash >> 31 | hash << 1;
    hash = hash ^ table256[*toHash];
    ++toHash;
  }

  free(base);
  return hash % table_size;
}

/* hash: compute hash value for array of NPREF strings */
unsigned int add3Hash(unsigned char* s[NPREF])
{
  unsigned char* toHash = flatten(s);
  unsigned char* base = toHash;
  unsigned int i = 0;
  unsigned int hash = 0;

  while (*toHash != '\0' && i < 3){
    hash += *toHash;
    ++i;
    ++toHash;
  }

  /* printf("%d\n\n", hash ); */

  free(base);
  return hash % table_size;
}

static void rehash(PrefixStorer* ps)
{
  int i = 0;
  int orig_table_size = table_size; 

  State** new_hashtable;
  State** statehead = (State**) (ps->tableBase);
  State* sp;
  
  table_size *= EXPNDFCT;
  entry_size = 0;
  new_hashtable = emalloc(table_size * sizeof(State*));

  for (i = 0; i < table_size; i++)
    new_hashtable[i] = NULL;

  ps->tableBase = new_hashtable;

  /* Clean up old table */
  for (i = 0; i < orig_table_size; i++) {
    sp = statehead[i];

    while (sp != NULL){
      State* current = sp;
      ht_add(sp->pref, sp->suf->word, ps);   
      sp = sp->next;
      cleanupState(current);
      free(current);
      
    }
    
  }

  free(statehead);
}

/*
 * Returns a pointer if the given prefix is present or created, else NULL.
 *
 * Parameters:
 *  prefix - Prefix to find.
 *  create - Create a new entity if not present.
 *  ps - PrefixStore implementation.
 *
 * Note: This function does not copy the input (strdup). Do not change the given string after insertion.
 */
static State* ht_lookup(unsigned char* prefix[NPREF], int create, PrefixStorer* ps)
{
  /*
   * Looks up the state, and optionally creates a new node depending
   * on what create has been set to.
   */
  
  int i;
  unsigned int indx;

  indx = (ps->hash)(prefix); 
  State* sp;
  State** statehead = (State**) (ps->tableBase);
  sp = statehead[indx];

  for (; sp != NULL; sp = sp->next) {

    for (i = 0; i < NPREF; i++)
      if (strcmp((char*) prefix[i], (char*) sp->pref[i]) != 0)
        break;
    if (i == NPREF)        /* found it */
      return sp;
  }

  if (create) {
    sp = (State*) emalloc(sizeof(State));
    for (i = 0; i < NPREF; i++)
      sp->pref[i] = prefix[i];
    sp->suf = NULL;
    sp->next = statehead[indx];
    statehead[indx] = sp;
    ++entry_size;
  }

 

  return sp;
}

/*
 * This calls your other functions you already implemented, so as long
 * as the other functions are done correctly, this should require no
 * changes.
 */

/* add: add word to suffix list, update prefix */
static void ht_add(unsigned char* prefix[NPREF], unsigned char* suffix, PrefixStorer* ps)
{
  /* delete this line when you finish the implementation */
  State* sp;
 
  sp = (ps->lookup)(prefix, 1, ps);  /* create if not found */
  addsuffix(sp, suffix);


  /* move the words down the prefix */
  memmove(prefix, prefix + 1, (NPREF - 1) * sizeof(prefix[0]));
  prefix[NPREF - 1] = suffix;

  if ( entry_size / table_size == load_factor ){
    rehash(ps);
  }

}
