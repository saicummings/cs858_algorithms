/**
 * \file sort_skele.c
 *
 * Sorts integers from a file using various algorithms.
 *  assignment 2
 * \author Saisuk Lekyang
 * \date 09-08-18
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/time.h>

/* The various sorting algorithm names */
static const char *counting_sort_str = "counting";
static const char *radix_sort_str = "radix";
static const char *quick_sort_str = "quick";
static const char *quick2_sort_str = "quick2";
static const char *insertion_sort_str = "insertion";
static const char *system_quick_sort_str = "system_quick";
static const char *merge_sort_str = "merge";
static const char *merge2_sort_str = "merge2";


/* random number gen from stackoverflow 
https://stackoverflow.com/questions/1640258/need-a-fast-random-generator-for-c
*/
unsigned long xorshf96(void) {        
    static unsigned long x=123456789, y=362436069, z=521288629;
    unsigned long t;
        x ^= x << 16;
        x ^= x >> 5;
        x ^= x << 1;

    t = x;
    x = y;
    y = z;
    z = t ^ x ^ y;

    return z;
}


/* Gets the time of day in seconds. */
static double get_current_seconds(void)
{
    double sec, usec;
    struct timeval tv;

    if (gettimeofday(&tv, NULL) < 0) {
    perror("gettimeofday failed");
    exit(EXIT_FAILURE);
    }

    sec = tv.tv_sec;
    usec = tv.tv_usec;

    return sec + (usec / 1000000);
}

/* Number of bits in an unsigned long. */
static const unsigned long ulong_bits = 8 * sizeof(unsigned long);
/************************************************************
 * Example: insertion sort
 ************************************************************/

/*
 * Insertion sort.
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_insertion_sort(unsigned long ary[], unsigned long n,
				      unsigned long nbits)
{
    unsigned long j;

    for (j = 1; j < n; j += 1) {
	unsigned long key = ary[j];
	unsigned long i = j;
	while (i > 0 && ary[i-1] > key) {
	    ary[i] = ary[i-1];
	    i -= 1;
	}
	ary[i] = key;
    }

    return 0;
}

/************************************************************
 * Functions that you must implement
 ************************************************************/

/*
 * Radix sort
 * Return 0 on success and 1 on failure.
*/
static unsigned int do_radix_sort(unsigned long ary[],
				  unsigned long n, unsigned long nbits)
{
    unsigned long * pos;
    unsigned long i;
    unsigned long j;
    
    pos = calloc(n, sizeof(unsigned long));

    for ( j=0; j < nbits; j += 4) {
        unsigned long count[16] = {0};

        /* loop through and increment count */
        for (i = 0; i < n; i++){
            unsigned long cur = ary[i];
            cur = cur & ( 15 << j);
            cur = cur >> j;
            count[(int)cur]++;
        }

        /* loop through set pos */
        for (i = 1; i < 16; i++){
            count[i] += count[i - 1];
        }

        /* iterate backwards to maintain stability */
        i = n;
        while (i != 0){
            unsigned long cur = ary[--i];
            cur = cur & ( 15 << j);
            cur = cur >> j;
            pos[count[cur] - 1] = ary[i];
            count[cur]--;
        }

        for (i = 0; i < n; i++){
            ary[i] = pos[i];
        }

    } 

    free(pos);
    return 0;
}


/*
 * Counting sort
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_counting_sort(unsigned long ary[],
				     unsigned long n, unsigned long nbits)
{
    unsigned long * count;
    unsigned long * pos;
    unsigned long array_size = 1 << nbits;
    long i;

    if (nbits > 20){
        exit(EXIT_FAILURE);
        return 1;
    }

    count = calloc(array_size, sizeof(unsigned long));
    pos = calloc(n, sizeof(unsigned long));

    /* loop through and increment count */
    for (i = 0; i < n; i++){
        count[(int)ary[i]]++;
    }

    /* loop through set pos */
    for (i = 1; i < array_size; i++){
        count[i] += count[i - 1];
    }

    /* iterate backwards to maintain stability */
    for (i = n - 1; i >= 0; i--){
        pos[count[ary[i]] - 1] = ary[i];
        count[ary[i]]--;
    }

    for (i = 0; i < n; i++){
        ary[i] = pos[i];
    }

    free(count);
    free(pos);
    return 0;
}


/*
 * Insertion sort helper
 * Return 0 on success and 1 on failure.
 */
static unsigned int helper_insertion_sort(unsigned long ary[],
                  unsigned long n, unsigned long m){
    unsigned long j;
    unsigned long i;
    
    for (j = n + 1; j <= m; j++) {
        unsigned long key = ary[j];

        i = j;
        while (i > n && ary[i-1] > key) {
            ary[i] = ary[i-1];
            i -= 1;
        }
        ary[i] = key;
    }
 
    return 0;
}

/*
 * Quicksort helper
 * Return 0 on success and 1 on failure.
 */
static unsigned int helper_quicksort(unsigned long ary[],
                  unsigned long i, unsigned long j){
    long p, y;
    long x, tmp;
    long size = j - i + 1;   

    /* set random seed 
    srand(get_current_seconds()); */
    p =  (xorshf96() % (size)) + i;
 
    /* partition */
    tmp = ary[j];
    ary[j] = ary[p];
    ary[p] = tmp;

    x = i;
    for(y = i; y <= j - 1; y++){
        if (ary[y] <= ary[j]){
            tmp = ary[x];
            ary[x] = ary[y];                  
            ary[y] = tmp;
            x++;
        }
    }
    tmp = ary[x];
    ary[x] = ary[j];
    ary[j] = tmp;
   
    if (x - 1 > (long signed)i){
        helper_quicksort(ary, i, x - 1);}
    if ((long signed)j > x + 1){
        helper_quicksort(ary, x + 1, j);}

    return 0;
}

/*
 * Quicksort
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_quicksort(unsigned long ary[],
				  unsigned long n, unsigned long nbits)
{
    helper_quicksort(ary, 0, n - 1);
    return(0);
    exit(EXIT_FAILURE);
}

/*
 * Quicksort2 helper
 * Return 0 on success and 1 on failure.
 */
static unsigned int helper_quicksort2(unsigned long ary[],
                  unsigned long i, unsigned long j){
    long p, y, p2, p3;
    long x, tmp;
    long size = j - i + 1;
    unsigned long k_max;

    k_max = 40;

    if (size < k_max)
        return 0;

    /* set random seed 
    srand(get_current_seconds()); */
    /* get 3 random numbers */
    p =  (xorshf96() % (size)) + i;
    p2 = (xorshf96() % (size)) + i;
    p3 = (xorshf96() % (size)) + i;
  
    /* get the median */
    if ((ary[p] <= ary[p2] && ary[p2] <= ary[p3])
        || (ary[p3] <= ary[p2] && ary[p2] <= ary[p])){
        p = p2;
    } else if ((ary[p] <= ary[p3] && ary[p3] <= ary[p2])
        || (ary[p2] <= ary[p3] && ary[p3] <= ary[p])){
        p = p3;
    } 
  
    /* partition */
    tmp = ary[j];
    ary[j] = ary[p];
    ary[p] = tmp;

    x = i;
    for(y = i; y <= j - 1; y++){
        if (ary[y] <= ary[j]){
            tmp = ary[x];
            ary[x] = ary[y];                  
            ary[y] = tmp;
            x++;
        }
    }
    tmp = ary[x];
    ary[x] = ary[j];
    ary[j] = tmp;
   
    if (x - 1 > (long signed)i){
        helper_quicksort2(ary, i, x - 1);
    }
    if ((long signed)j > x + 1){
        helper_quicksort2(ary, x + 1, j);
    }

    return 0;
}

/*
 * Quicksort2
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_quicksort2(unsigned long ary[],
                  unsigned long n, unsigned long nbits)
{
    
    helper_quicksort2(ary, 0, n - 1);
    do_insertion_sort(ary, n, nbits);
    return(0);
    exit(EXIT_FAILURE);
}

/*
 * Helper merge sort
 * Return 0 on success and 1 on failure.
 */
static unsigned int helper_merge_sort(unsigned long ary[], unsigned long i, 
                unsigned long j){
    unsigned long k, cr1, cr2;
    long n;
    unsigned long tmp;
    unsigned long * count;

    if (i >= j)
        return 0;

    k = (i + j)/2;
    helper_merge_sort(ary, i, k);
    helper_merge_sort(ary, k + 1, j);

    tmp = j - i + 1;

    /* creating tmp array to merge the "2 arrays" */
    count = calloc(tmp, sizeof(unsigned long));
    
    cr1 = i;
    cr2 = k + 1;

    for (n = 0; n < tmp; n++){
        if (cr2 <= j && cr1 <= k && ary[cr1] <= ary[cr2]){
            count[n] = ary[cr1++];
        } else if ( cr2 <= j ) {
            count[n] = ary[cr2++];
        } else if ( cr1 > k) {
            count[n] = ary[cr2++];
        } else if ( cr2 > j) {
            count[n] = ary[cr1++];
        }
    }

    for (n = 0; n < tmp; n++){
        ary[i + n] = count[n];
    }

    free(count);
    return 0;
}

/*
 * Merge sort
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_merge_sort(unsigned long ary[],
                  unsigned long n, unsigned long nbits)
{
    helper_merge_sort(ary, 0, n - 1);
    return 0;
    exit(EXIT_FAILURE);
}

/*
 * Helper merge2 sort
 * Return 0 on success and 1 on failure.
 */
static unsigned int helper_merge2_sort(unsigned long ary[], unsigned long i, 
                    unsigned long j){
    unsigned long k, cr1, cr2;
    long n;
    unsigned long tmp, k_max;
    unsigned long * count;

    k_max = 20;
    tmp = j - i + 1;

    if ( tmp < k_max){
      
        helper_insertion_sort(ary, i, j);
   
        return 0;
    }

    k = (i + j)/2;
    helper_merge2_sort(ary, i, k);
    helper_merge2_sort(ary, k + 1, j);


    /* creating tmp array to merge the "2 arrays" */
    count = calloc(tmp, sizeof(unsigned long));
    
    cr1 = i;
    cr2 = k + 1;

    for (n = 0; n < tmp; n++){
        if (cr2 <= j && cr1 <= k && ary[cr1] <= ary[cr2]){
            count[n] = ary[cr1++];
        } else if ( cr2 <= j ) {
            count[n] = ary[cr2++];
        } else if ( cr1 > k) {
            count[n] = ary[cr2++];
        } else if ( cr2 > j) {
            count[n] = ary[cr1++];
        }
    }

    for (n = 0; n < tmp; n++){
        ary[i + n] = count[n];
    }

    free(count);

    return 0;
}

/*
 * Merge2 sort
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_merge2_sort(unsigned long ary[],
                  unsigned long n, unsigned long nbits)
{
    helper_merge2_sort(ary, 0, n - 1);
    return 0;
    exit(EXIT_FAILURE);
}


/************************************************************
 *
 * You probably don't need to modify anything beyond here.
 *
 ************************************************************/


/************************************************************
 * Example: Using the standard library's qsort() routine.
 ************************************************************/

/* The comparison function for qsort(). */
static int compare(const void *_a, const void *_b)
{
    unsigned long *a = (unsigned long *) _a;
    unsigned long *b = (unsigned long *) _b;

    return *a - *b;
}


/*
 * Uses the standard library quicksort function.
 * Return 0 on success and 1 on failure.
 */
static unsigned int do_system_quicksort(unsigned long ary[],
					unsigned long n,
					unsigned long nbits)
{
    /* This is the system's quick sort function. */
    qsort(ary, n, sizeof(*ary), compare);

    return 0;
}

/*
 * Read the header from the input file and returts the number of
 * values in the input using the 'nvalues' argument and the number of
 * bits for each number using the 'nbits' argument.
 *
 * Returns 0 on success and 1 on failure.
 */
static unsigned int read_file_header(FILE * infile, unsigned long *nvalues,
				     unsigned long *nbits)
{
    int ret;
    unsigned long _nvalues, _nbits;

    ret = fscanf(infile, " %lu %lu", &_nvalues, &_nbits);
    if (ret == EOF) {
	fprintf(stderr, "Unexpected end of file\n");
	return 1;
    }
    if (ret != 2) {
	fprintf(stderr, "Malformed file header\n");
	return 1;
    }

    if (_nbits > ulong_bits) {
	fprintf(stderr, "%lu bits input values are too big\n", _nbits);
	fprintf(stderr, "Word size seems to be %lu bits\n", ulong_bits);
	return 1;
    }

    if (nvalues)
	*nvalues = _nvalues;
    if (nbits)
	*nbits = _nbits;

    return 0;
}

/*
 * Reads the next number from the input file into the 'num' argument.
 * If the end of the file is reached then 'num' is left as is and
 * 'eof' is set to 1, otherwise 'eof' is set to zero.
 *
 * Returns 0 on success and 1 on failure.
 */
static unsigned int read_next_number(FILE * infile, unsigned long *num,
				     unsigned int *eof)
{
    int ret;
    unsigned long _num;

    ret = fscanf(infile, " %lu", &_num);
    if (ret == EOF) {
	*eof = 1;
	return 0;
    }
    if (ret != 1) {
	perror("fscanf failed");
	return 1;
    }

    *num = _num;
    *eof = 0;

    return 0;
}

/*
 * Reads 'n' numbers from the given input file into the provided
 * array.
 *
 * Returns 0 on success and 1 on failure.  The state of 'ary' on
 * failure is unspecified.
 */
static unsigned int read_into_array(FILE * infile, unsigned long n,
				    unsigned long ary[])
{
    unsigned long i;

    for (i = 0; i < n; i += 1) {
	unsigned int err, eof;
	err = read_next_number(infile, &ary[i], &eof);
	if (err)
	    return 1;
	if (eof) {
	    fprintf(stderr, "Unexpected EOF when reading %lu values", n);
	    return 1;
	}
    }

    return 0;
}

/* Writes the given number to the output file. */
static void output_number(FILE * outfile, unsigned long num)
{
    fprintf(outfile, "%lu\n", num);
}

/* Output the given array to the output file. */
static void output_from_array(FILE * outfile, unsigned long ary[],
			      unsigned long n)
{
    unsigned long i;

    for (i = 0; i < n; i += 1)
	output_number(outfile, ary[i]);
}

/* Print the usage string */
static void usage(void)
{
    fprintf(stderr, "usage: sort <algorithm> <infile> <outfile>\n");
    fprintf(stderr,
	    "Where <algorithm> is one of: counting, radix, quick,\n"
	    "                             insertion or system_quick\n"
	    "and <infile> and/or <outfile> may be '-' to indicate that\n"
	    "the standard input and/or output stream should be used\n");
}


/*
 * Reads the file header and the values.  The return value is an array
 * of values that must be freed by the caller.
 */
static unsigned long *get_values(FILE * infile, unsigned long *n,
				 unsigned long *nbits)
{
    unsigned int err;
    unsigned long _n, _nbits;
    unsigned long *ary;

    err = read_file_header(infile, &_n, &_nbits);
    if (err)
	return NULL;

    ary = malloc(sizeof(*ary) * _n);
    if (!ary) {
	perror("Failed to allocate array");
	return NULL;
    }

    err = read_into_array(infile, _n, ary);
    if (err) {
	free(ary);
	return NULL;
    }

    if (n)
	*n = _n;
    if (nbits)
	*nbits = _nbits;

    return ary;
}

/*
 * Reads the values, begins the timer, calls the sorting algorithm,
 * stops the timer and outputs the values.  The time taken is printed
 * to standard error.
 */
static unsigned int do_sort(const char *const algorithm, FILE * infile,
			    FILE * outfile)
{
    int err = 0;
    double start, end;
    unsigned long n, nbits;
    unsigned long *ary;

    ary = get_values(infile, &n, &nbits);
    if (!ary)
	return 1;

    start = get_current_seconds();

    if (strcmp(algorithm, counting_sort_str) == 0) {
	err = do_counting_sort(ary, n, nbits);

    } else if (strcmp(algorithm, radix_sort_str) == 0) {
	err = do_radix_sort(ary, n, nbits);

    } else if (strcmp(algorithm, quick_sort_str) == 0) {
	err = do_quicksort(ary, n, nbits);

    } else if (strcmp(algorithm, quick2_sort_str) == 0) {
    err = do_quicksort2(ary, n, nbits);

    } else if (strcmp(algorithm, radix_sort_str) == 0) {
    err = do_radix_sort(ary, n, nbits);

    } else if (strcmp(algorithm, merge_sort_str) == 0) {
    err = do_merge_sort(ary, n, nbits);

    } else if (strcmp(algorithm, merge2_sort_str) == 0) {
    err = do_merge2_sort(ary, n, nbits);
    
    } else if (strcmp(algorithm, insertion_sort_str) == 0) {
	err = do_insertion_sort(ary, n, nbits);

    } else if (strcmp(algorithm, system_quick_sort_str) == 0) {
	err = do_system_quicksort(ary, n, nbits);

    } else {
	fprintf(stderr, "Impossible\n");
	exit(EXIT_FAILURE);
    }

    end = get_current_seconds();

    output_from_array(outfile, ary, n);
    fprintf(stderr, "%f\n", end - start);

    free(ary);

    return err;
}


int main(int argc, char *const argv[])
{
    int ret = EXIT_SUCCESS;
    unsigned int err;
    FILE *infile = stdin, *outfile = stdout;

    if (argc < 4 || (strcmp(argv[1], counting_sort_str) != 0
		     && strcmp(argv[1], radix_sort_str) != 0
		     && strcmp(argv[1], quick_sort_str) != 0
             && strcmp(argv[1], quick2_sort_str) != 0
             && strcmp(argv[1], merge_sort_str) != 0
             && strcmp(argv[1], merge2_sort_str) != 0
		     && strcmp(argv[1], insertion_sort_str) != 0
		     && strcmp(argv[1], system_quick_sort_str) != 0)) {
	usage();
	return EXIT_FAILURE;
    }


    if (strcmp(argv[2], "-") != 0) {
	infile = fopen(argv[2], "r");
	if (!infile) {
	    perror("Failed to open input file for reading");
	    ret = EXIT_FAILURE;
	    goto out;
	}
    }

    if (strcmp(argv[3], "-") != 0) {
	outfile = fopen(argv[3], "w");
	if (!outfile) {
	    perror("Failed to open output file for writing");
	    ret = EXIT_FAILURE;
	    goto out;
	}
    }

    err = do_sort(argv[1], infile, outfile);
    if (err)
	ret = EXIT_FAILURE;

  out:

    if (outfile && outfile != stdout)
	fclose(outfile);
    if (infile && infile != stdin)
	fclose(infile);

    return ret;
}
