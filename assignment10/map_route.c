/**
 * \file map_route.c
 *
 * Plans a route in a roadmap.
 *
 * \author eaburns
 * \date 18-08-2010
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "roadmap.h"
#include "system.h"

void static heapup(int * heap_wt, int * heap_track, int * heap, int heap_loc, int heap_size){
    int parent = (heap_loc-1)/2, temp;

	if(heap_loc != 0 && heap_wt[heap[parent]] > heap_wt[heap[heap_loc]]){
		temp = heap[heap_loc];
		heap[heap_loc] = heap[parent];
        heap_track[heap[parent]] = heap_loc;
        heap[parent] = temp;
        heap_track[temp] = parent;
		heapup(heap_wt, heap_track, heap, parent, heap_size);
	} 
}

void static heapdown(int* heap_wt, int * heap_track, int * heap, int heap_loc, int heap_size){
    int l, r, smallest, temp;
	l = 2*heap_loc + 1;
	r = 2*heap_loc + 2;

	if(l < heap_size && heap_wt[heap[l]] < heap_wt[heap[heap_loc]]){
		smallest = l;
	} else {
		smallest = heap_loc;
	}

	if (r < heap_size && heap_wt[heap[r]] < heap_wt[heap[smallest]])
		smallest = r;

	if(smallest != heap_loc){
		temp = heap[heap_loc];
		heap[heap_loc] = heap[smallest];
		heap_track[heap[smallest]] = heap_loc;

		heap[smallest] = temp;
		heap_track[temp] = smallest;

		heapdown(heap_wt, heap_track, heap, smallest, heap_size);
	
	}
}

/*
 * Single source shortest path solver using Djikstra's algorithm.
 * 'source' is the ID value of the source node (its node structure can
 * be found at nodes[source - 1]) and 'target' is the ID value of the
 * target node.  The 'costp' argument should be used to return the
 * final path cost value.
 *
 * Return 0 on success and 1 on failure.
 */
int djikstra(FILE * outfile, struct node nodes[], unsigned int nnodes,
	     unsigned int source, unsigned int target, unsigned int *costp)
{
    /* Find the shortest path and output it.  Return the cost via
     * 'costp'. */
    int i;
    int inf = 0x7FFFFFFF;
    int heap_size = nnodes;
    int * heap = malloc(sizeof(int) * nnodes);
    int * heap_track = malloc(sizeof(int) * nnodes);
    int * heap_wt = malloc(sizeof(int) * nnodes);
    int * prev = malloc(sizeof(int) * nnodes);
    char * in_q = malloc(sizeof(char) * nnodes);


    for(i = 0; i < nnodes; i++){
        heap_wt[i] = inf;
        heap[i] = i;
        heap_track[i] = i;
        prev[i] = -1;
        in_q[i] = 1;
    }

    heap_wt[source] = 0;
    heap[0] = source;
    heap[source] = 0;
    heap_track[source] = 0;
    heap_track[0] = source;

    while( heap_size > 0){
        struct node * u = &nodes[heap[0]];
        int uid = u->num;
        unsigned int narcs = u->narcs;
        struct arc *arcv = u->arcv;

        in_q[uid] = 0;

        if ( u->num == target)
            break;

        heap[0] = heap[heap_size - 1];
		heap_track[heap[heap_size - 1]] = 0;

        heap_size--;
        heapdown(heap_wt, heap_track, heap, 0, heap_size);

        for(i = 0; i < narcs; i++){
            int v = arcv[i].target;
                  
            if (in_q[v] == 1 && heap_wt[v] > heap_wt[uid] + arcv[i].wt){
                heap_wt[v] = heap_wt[uid] + arcv[i].wt;
                prev[v] = uid;
                heapup(heap_wt, heap_track, heap, heap_track[v], heap_size);
            }
        }
    }

    *costp = heap_wt[target]; 
    i = 0;
    while(target != source){
        heap[i++] = target;
        target = prev[target];
    }  heap[i] = target;

    while(i >= 0){
        printf("%d\n",heap[i--]);
    }

    free(heap);
    free(heap_track);
    free(heap_wt);
    free(prev);
    free(in_q);
    return 0;
}



/* Read the user's input and call the search. */
static int input_and_search(FILE * infile, struct node nodes[],
			    unsigned int nnodes)
{
    int err = 0;
    unsigned int s, t;
    unsigned int cost = 0;
    double start, end;

    while (fscanf(infile, "%u %u", &s, &t) == 2) {
	s = s - 1; /* avoiding 1-indexing */
	t = t - 1;
	if (s >= nnodes) {
	    fprintf(stderr, "Start node is invalid\n");
	    continue;
	}
	if (t >= nnodes) {
	    fprintf(stderr, "Target node is invalid\n");
	    continue;
	}
	printf("finding a route from %d to %d\n", s, t);
	start = get_current_seconds();
	err = djikstra(stdout, nodes, nnodes, s, t, &cost);
	end = get_current_seconds();
	if (err)
	    break;
	printf("cost: %u\n", cost);
	printf("time: %f seconds\n", end - start);
    }

    return err;
}


/* Print the usage string. */
static void usage(void)
{
    fprintf(stderr, "Usage:\nmap-route <datafile> <file>\n");
    exit(EXIT_FAILURE);
}


int main(int argc, const char *const argv[])
{
    int err, ret = EXIT_FAILURE;
    FILE *f, *infile = stdin;
    double start, end;
    unsigned int nnodes;
    struct node *nodes;

    if (argc != 3)
	usage();

    f = fopen(argv[1], "r");
    if (!f) {
	perror("Error opening data file");
	return EXIT_FAILURE;
    }
    if (strcmp(argv[2], "-") != 0) {
	infile = fopen(argv[2], "r");
	if (!infile) {
	    perror("Error opening data file");
	    fclose(f);
	    return EXIT_FAILURE;
	}
    }

    start = get_current_seconds();
    err = load_map(f, &nodes, &nnodes);
    if (err)
	goto out_map;
    end = get_current_seconds();
    fclose(f);
    if (err)
	return EXIT_FAILURE;
    printf("Loaded %d nodes in %f seconds\n", nnodes, end - start);
    printf("Using %d MB\n", peak_memory_usage());

    err = input_and_search(infile, nodes, nnodes);
    if (err)
	goto out;

    printf("Peak memory usage %d MB\n", peak_memory_usage());

    ret = EXIT_SUCCESS;
  out:
    free_map(nodes, nnodes);
  out_map:
    if (infile != stderr)
	fclose(infile);
    return ret;
}
