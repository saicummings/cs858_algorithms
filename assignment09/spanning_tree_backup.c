/**
 * \file spanning_tree.c
 *
 *
 *
 * \author jtd7
 * \date 20-10-2011
 */

#define _POSIX_C_SOURCE 200112L
#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

void usage(){
	fprintf(stderr, "Usage:\ntree <infile> <max_distance>\n");
	exit(EXIT_FAILURE);
}

void heapify(double* v_weight,int * a, int i, int n){
	int l, r, smallest, temp;
	l = 2*i + 1;
	r = 2*i + 2;

	if(l < n && v_weight[a[l]] < v_weight[a[i]]){
		smallest = l;
	} else {
		smallest = i;
	}

	if (r < n && v_weight[a[r]] < v_weight[a[smallest]])
		smallest = r;

	if(smallest != i){
		temp = a[i];
		a[i] = a[smallest];
		a[smallest] = temp;
	}
}


int solve(struct graph* g){
	int i, j;
	int size = g->v_size;
	int heap_size = size;
	double cost = 0;

	int * minheap = malloc(sizeof(unsigned int) * size);
	for(i = 0; i < size; i++){
		minheap[i] = i;
	}

	/* modified Prim */
	g->v_weight[0] = 0;
	while (heap_size > 0){
		struct v_node * u_adj;
		int u = minheap[0];
		int parent = g->v_parent[u];

		u_adj = g->adj_list[u];

		/* remove min */
		g->v_in_q[u] = 0;

		if (parent != -1){
			printf("%d %d\n", parent, u);
			cost += g->v_weight[u];			
		}
		minheap[0] = minheap[heap_size - 1];
		heap_size--;

		/* iterate through u's neighbors */
		while(u_adj){
			int v = u_adj->id;
			double w = u_adj->cost;
			if (g->v_in_q[v] == 1 && w < g->v_weight[v]){
				g->v_parent[v] = u;
				g->v_weight[v] = w;
			}
			u_adj = u_adj->next;	
		}
		if(g->adj_list[u]){
			for(j = heap_size/2; j >= 0; j--){
				heapify(g->v_weight, minheap, j, heap_size);
			}
		}
	}

	printf("%f\n", cost);
	free(minheap);
	return 0;
}


int process(const char* infile, double max_distance){
	float *x_array;
	float *y_array;
	int num_points = -1;
	FILE *ifile = fopen(infile, "r");
	char buf[256];
	char c = '\0';
	int b_index = 0;
	int p_index = 0;
	float x = 0.;
	float y = 0.;
	int on_x = 1;
	struct points* p;
	struct graph* g;
	int err = 0;
	int i;

	buf[0] = '0';
	buf[1] = '\0';

	if(ifile == NULL){ /* fopen failed */
		fprintf(stderr, "Failed to open %s\n", infile);
		return EXIT_FAILURE;
	}else{  /* get input size */
		while((c = getc(ifile)) != '\n'){
			buf[b_index] = c;
			b_index++;
		}
		num_points = atoi(buf);
		x_array = malloc(sizeof(float) * num_points);
		y_array = malloc(sizeof(float) * num_points);
		b_index = 0;
		while((c = getc(ifile)) != EOF){
			buf[b_index] = c;
			if(c == '\n' || c == ' '){
				if (on_x){
					x = atof(buf);
					on_x = 0;
				}else{
					y = atof(buf);
					x_array[p_index] = x;
					y_array[p_index] = y;
					p_index ++;
					on_x = 1;
				}
				b_index = 0;
			}else b_index++;
		}
	}
	p = newPoints(x_array, y_array, num_points);
	g = newGraph(p, max_distance);

	/* solve the graph */
	err = solve(g);
	/* cleanup here */

	free(g->v_weight);
	free(g->v_in_q);
	free(g->v_parent);

	for(i = 0; i < g->v_size; i++){
		struct v_node * cur = g->adj_list[i];
		struct v_node * temp;
		if (cur){
			while(cur){
				temp = cur;
				cur = cur->next;
				free(temp);
			}
		}
	}

	free(g->adj_list);
	free(p);
	free(x_array);
	free(y_array);
	free(g);

	fclose(ifile);
	return err;
}

int main(int argc, const char *const argv[]){
	double d;
	if (argc != 3)	usage();
	d = atof(argv[2]);
	return process(argv[1], d);
}
