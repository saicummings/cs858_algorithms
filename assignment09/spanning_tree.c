/**
 * \file spanning_tree.c
 *
 *
 *
 * \author jtd7/ sai lekyang
 * \date 20-10-2011
 */

#define _POSIX_C_SOURCE 200112L
#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int top_heap = -1;

void usage(){
	fprintf(stderr, "Usage:\ntree <infile> <max_distance>\n");
	exit(EXIT_FAILURE);
}

void static heapup(double * heap_wt, int * heap_track, int * heap, int heap_loc, int heap_size){
    int parent = (heap_loc-1)/2, temp;

	if(heap_loc != 0 && heap_wt[heap[parent]] > heap_wt[heap[heap_loc]]){
		temp = heap[heap_loc];
		heap[heap_loc] = heap[parent];
        heap_track[heap[parent]] = heap_loc;
        heap[parent] = temp;
        heap_track[temp] = parent;
		heapup(heap_wt, heap_track, heap, parent, heap_size);
	} 
}

void static heapdown(double* heap_wt, int * heap_track, int * heap, int heap_loc, int heap_size){
    int l, r, smallest, temp;
	l = 2*heap_loc + 1;
	r = 2*heap_loc + 2;

	if(l < heap_size && heap_wt[heap[l]] < heap_wt[heap[heap_loc]]){
		smallest = l;
	} else {
		smallest = heap_loc;
	}

	if (r < heap_size && heap_wt[heap[r]] < heap_wt[heap[smallest]])
		smallest = r;

	if(smallest != heap_loc){
		temp = heap[heap_loc];
		heap[heap_loc] = heap[smallest];
		heap_track[heap[smallest]] = heap_loc;

		heap[smallest] = temp;
		heap_track[temp] = smallest;

		heapdown(heap_wt, heap_track, heap, smallest, heap_size);
	
	}
}


int solve(struct graph* g){
	int i;
	int size = g->v_size;
	int heap_size = size;
	double cost = 0;

	int * heap = malloc(sizeof(unsigned int) * size);
	int * heap_track = malloc(sizeof(unsigned int) * size);

	/* modified Prim */
	g->v_weight[0] = 0;

	for(i = 0; i < size; i++){
		heap[i] = i;
		heap_track[i] = i;
	} 

	while (heap_size > 0){
		struct v_node * u_adj;
		int u = heap[0];
		int parent = g->v_parent[u];

		u_adj = g->adj_list[u];

		/* remove min */
		g->v_in_q[u] = 0;

		if (parent != -1){
			printf("%d %d\n", parent, u);
			cost += g->v_weight[u];		
			/* printf("%f\n" , g->v_weight[u] ); */
		}

		heap[0] = heap[heap_size - 1];
		heap_track[heap[heap_size - 1]] = 0;
		heap_size--;
		heapdown(g->v_weight, heap_track, heap, 0, heap_size);

		/* iterate through u's neighbors */
		while(u_adj){
			int v = u_adj->id;
			double w = u_adj->cost;
			if (g->v_in_q[v] == 1 && w < g->v_weight[v]){
				/* printf("updating node %d\n", v); */
			
				g->v_parent[v] = u;
				g->v_weight[v] = w;
				heapup(g->v_weight, heap_track, heap, heap_track[v], heap_size);
			}
			u_adj = u_adj->next;
			/*heapify(g->v_weight, heaptrack, minheap, 0, heap_size);*/
		}
		/*for(i = heap_size / 2;i >= 0;--i)
			heapify(g->v_weight, heaptrack, minheap, i, heap_size);*/

	}

	printf("%f\n", cost);
	free(heap);
	free(heap_track);
	return 0;
}


int process(const char* infile, double max_distance){
	float *x_array;
	float *y_array;
	int num_points = -1;
	FILE *ifile = fopen(infile, "r");
	char * buf = calloc(256, sizeof(char)); /*[256];*/
	char c = '\0';
	int b_index = 0;
	int p_index = 0;
	float x = 0.;
	float y = 0.;
	int on_x = 1;
	struct points* p;
	struct graph* g;
	int err = 0;
	int i;

	buf[0] = '0';
	buf[1] = '\0';

	if(ifile == NULL){ /* fopen failed */
		fprintf(stderr, "Failed to open %s\n", infile);
		return EXIT_FAILURE;
	}else{  /* get input size */
		while((c = getc(ifile)) != '\n'){
			buf[b_index] = c;
			b_index++;
		}
		num_points = atoi(buf);
		x_array = malloc(sizeof(float) * num_points);
		y_array = malloc(sizeof(float) * num_points);
		b_index = 0;
		while((c = getc(ifile)) != EOF){
			buf[b_index] = c;
			if(c == '\n' || c == ' '){
				if (on_x){
					x = atof(buf);
					on_x = 0;
				}else{
					y = atof(buf);
					x_array[p_index] = x;
					y_array[p_index] = y;
					p_index ++;
					on_x = 1;
				}
				b_index = 0;
			}else b_index++;
		}
	}
	p = newPoints(x_array, y_array, num_points);
	g = newGraph(p, max_distance);

	/* solve the graph */
	err = solve(g);
	/* cleanup here */

	free(g->v_weight);
	free(g->v_in_q);
	free(g->v_parent);

	for(i = 0; i < g->v_size; i++){
		struct v_node * cur = g->adj_list[i];
		struct v_node * temp;
		if (cur){
			while(cur){
				temp = cur;
				cur = cur->next;
				free(temp);
			}
		}
	}

	free(g->adj_list);
	free(p);
	free(x_array);
	free(y_array);
	free(g);
	free(buf);
	fclose(ifile);
	return err;
}

int main(int argc, const char *const argv[]){
	double d;
	if (argc != 3)	usage();
	d = atof(argv[2]);
	return process(argv[1], d);
}
