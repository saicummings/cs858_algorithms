/**
 * \file graph.h
 *
 *
 *
 * \author jtd7/ sai lekyang
 * \date 20-10-2011
 */

#ifndef GRAPH_H
#define GRAPH_H

#include <stdlib.h>
#include <math.h>

/* I do a lot of dynamic resizing arrays in my code.
   I used 1.5 instead of 2 to avoid overshooting my end memory too much. */
const static float ar_scale = 1.5;

/* rather than storing an array of tuples, I store two arrays,
   one for x, and one for y. */
struct points {
	float *xs;
	float *ys;
	int num_points;
};

struct v_node {
	int id;
	double cost;
	struct v_node * next;
};

struct graph {
/* your fields here */
	
	double * v_weight;
	unsigned char * v_in_q;
	int * v_parent;
	struct v_node ** adj_list;
	unsigned int v_size;

	/*
	int * mst_p;
	int * mst_v;
	unsigned int mst_size;
	*/
};

struct edges {
/* your fields here */

	double cost;
	int start;
	int end;

};

/* an auxilliary data structure that has nothing to do with your code,
   used for making all of the valid edges efficiently */
struct vect {
	int* elms;
	int num_elms;
	int index;
};

struct points* newPoints(float *xs, float *ys, int np);
struct edges* makeEdges(struct points *p, double max_dist, struct graph* g);
struct graph* newGraph(struct points* p, double max_dist);
int addEdge(struct edges* e, double cost, int start, int end, struct graph* g);
void add_vert(struct v_node ** s, double cost, int start, int end);

#endif
