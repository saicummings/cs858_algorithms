/**
 * \file flow_net.c
 *
 *
 *
 * \author jtd7
 * \date 07-11-2011
 */

#include "flow_net.h"
#include <limits.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

extern int dest, source;

int sumFlowIn(struct net* n, int id);
int sumFlowOut(struct net* n, int id);
void  checkAllFlow(struct net* n);

char static bfs(struct net* n, int s, int t){
  int i, u, head = 0, size = 0;
  memset(n->visited, 0, n->regular_nodes * sizeof(char));
  n->q[size++] = s;
  n->visited[s] = 1;

  while(head < size){
    u = n->q[head++];
    for(i = 0; i < n->regular_nodes; i++){
      if ( n->visited[i] == 0 && n->graph[u][i] > 0 ){
        n->visited[i] = 1;
        n->parent[i] = u;
        n->q[size++] = i;
      }
    }
  }

  return n->visited[t];
}

int net_add_arc(struct net *n, int antip, unsigned int s, 
    unsigned int t,	int cap){
  n->graph[s][t] = cap;
  return 0;
}

int net_free(struct net *n){
  int i;
  free(n->parent);
  free(n->visited);
  free(n->q);
  for (i = 0; i < n->regular_nodes; i++ ){
    free(n->graph[i]);
  }
  free(n->graph);
  return 1;
}

int init_flow_net(struct net* n, int n_nodes, int n_edges){
  int i;
  /* struct node* nodes = malloc(sizeof(struct node) * n_nodes); */
  n->graph = malloc(sizeof(int*)*n_nodes);  
  n->visited = calloc(n_nodes, sizeof(char));
  n->q = calloc(n_nodes * 2, sizeof(int));
  n->parent = malloc(sizeof(int) * n_nodes);

  for(i = 0; i < n_nodes; i++){
    /* nodes[i].id = i; */ 
    n->parent[i] = -1;
    n->graph[i] = calloc(n_nodes, sizeof(int));
  }
  
  n->regular_nodes = n_nodes;
  return 1;
}

int compute_max_flow(struct net* n){
  int max_flow = 0;
  int sink = 1;
  int source = 0;
  int i, j;
  int** org_graph = malloc(sizeof(int*)*n->regular_nodes);  

  for ( i = 0; i < n->regular_nodes; i++){
    org_graph[i] = malloc(sizeof(int)*n->regular_nodes); 
    for ( j = 0; j < n->regular_nodes; j++){
      org_graph[i][j] = n->graph[i][j]; 
    } 
  } 

  while(bfs(n, source, sink)){
    int path_flow = 0x7FFF;
    int s = sink;
    int v;

    while (s != source){
      int p = n->parent[s];
      if (p == -1) {
        path_flow = 0;
      } else if (path_flow > n->graph[p][s] ){
        path_flow = n->graph[p][s];
      }
      s = p;
    }

    max_flow += path_flow;
    v = sink;

    while ( v != source ){
      int u = n->parent[v];
      /* printf("%d %d %d\n", u, v, path_flow); */
      n->graph[u][v] -= path_flow;
      n->graph[v][u] += path_flow;
      v = n->parent[v];
    }
  }
  
  for ( i = 0; i < n->regular_nodes; i++){
    for (j = 0; j < n->regular_nodes; j++){
      int sum = n->graph[i][j] - org_graph[i][j];
      if (sum > 0){
        printf("%d %d %d\n", j, i, sum);
      } 
    } 
  } 

  printf("%d\n", max_flow);

  for (i = 0; i < n->regular_nodes; i++ ){
    free(org_graph[i]);
  }
  free(org_graph);

  return 1;
}

