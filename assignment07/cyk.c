



    #include <stdlib.h>
    #include <string.h>
    #include <assert.h>
    #include <stdio.h>
    #include <stdbool.h>

    #include "cyk.h"
    #include "parser.h"

    #include "rdp.h"
    /*
    pick up the globals from the parser
    */
    extern const rule* rule_list;
    extern const word_rule* word_list;
    extern int rule_count;
    extern int word_count;
    extern char* tokenized_input [MAX_INPUT];
    extern char input[MAX_CHARS];
    extern int input_length;
    extern int symbol_count;
    extern char** symbol_table;
    extern int start_index;

    typedef struct n{
        const char* val;
        struct n* next;
        struct n* l;
        struct n* r;
        double probability;
    } cyk_node;

    static cyk_node* new_cyk_node(){
        cyk_node * n = malloc(sizeof(cyk_node));
        n->val = NULL;
        n->next = NULL;
        n->l = NULL;
        n->r = NULL;
        return n;
    }

    /*
    static unsigned int contains(cyk_node * loc, const char* val){
        cyk_node* cur = loc;
        while( cur != NULL){
            cur = cur->next;
        }
        if(cur){
            return 0;
        }

        return 1;
    }
    */

    /* add node to the table and return the newly added node */
    static cyk_node* cyk_add(cyk_node * loc, const char* val, double prob){
        if(loc->val == NULL){
            loc->val = val;
            loc->probability = prob;
            return loc;
        } else {
            cyk_node* cur = loc;
            while( cur->next != NULL){
                cur = cur->next;
            }
            cur->next = new_cyk_node();
            cur->next->val = val;
            cur->next->probability = prob;
            return cur->next;
        }
        
    }

    static void print_parse(cyk_node * n){
        

        if (!n->l){
            printf("%s", n->val);
            return;
        }

        printf("%s(", n->val);
        print_parse(n->l);

        if (n->r){
            printf(" ");
            print_parse(n->r);
        }
        printf(")");
        
    }

    void cyk_parse(void){
        int i,r,c;
        double best_prob = 0;
        cyk_node *** table = malloc(sizeof(cyk_node**) * input_length);
        cyk_node ** sentence = malloc(sizeof(cyk_node*) * input_length);
        cyk_node * s;
        cyk_node * best_s = NULL;

        /* init */
        for(r = 0; r < input_length; r++){
            table[r] = malloc(sizeof(cyk_node*) * input_length - r);
        }
        for(r = 0; r < input_length; r++){
            for(c = 0; c < input_length - r; c++){
                table[r][c] = new_cyk_node();
            }
        }

        /* first parse */
        for(i = 0; i < input_length; i++){
            char* cur_word = tokenized_input[i];
            sentence[i] = new_cyk_node();
            sentence[i]->val = cur_word;
            const word_rule* current_terminal = word_list;

            while(current_terminal != NULL){
                if(strcmp(tokenized_input[i], current_terminal->word) == 0){
                    cyk_node* n = cyk_add(table[0][i], current_terminal->lhs, current_terminal->probability);
                    n->l = sentence[i];
                }
                current_terminal = current_terminal->next;
            }
        }

        /* parse using algo */
        for(r = 1; r < input_length; r++){
            for(c = 0; c < input_length - r; c++){
                int s, r2, c2;
                r2 = r;
                c2 = c;
                for(s = 0; s < r; s++){
                    cyk_node* rh1 = table[s][c];
                    cyk_node* rh2 = table[--r2][++c2];
                    cyk_node* rhs1 = rh1;
                    while(rhs1 != NULL ){
                        cyk_node* rhs2 = rh2;
                        while(rhs2 != NULL){
                            const rule* next_rule = rule_list;
                            while(next_rule != NULL){

                                
                                if(rhs1->val == NULL || rhs2->val == NULL){
                                    next_rule = next_rule->next;
                                    continue;
                                }

                                if(strcmp(next_rule->rhs1, rhs1->val) == 0 && 
                                        (strcmp(next_rule->rhs2, rhs2->val)) == 0){
                                    double prob;
                                    prob = rhs1->probability * rhs2->probability * next_rule->probability;
                                    cyk_node* n = cyk_add(table[r][c], next_rule->lhs, prob);
                                    n->l = rhs1;
                                    n->r = rhs2;
                                }
                                next_rule = next_rule->next;
                            }
                            rhs2 = rhs2->next;
                        }
                        rhs1 = rhs1->next;
                    }
                }
            }
        }
        

        if (1 == 1){
            for(c = 0; c < input_length; c++){
                cyk_node* cur = NULL;
                int i = 0;
                cur = sentence[c];
                while(cur != NULL){
                    /* printf("%s %d,", cur->val, (int)(cur->probability * 100)); */
                    
                    if (cur->val){
                        const char * s = cur->val;
                        
                        while(s[i] != '\0' && i < 8){
                            printf("%c",s[i++]);
                        }
                    }

                    cur = cur->next;
                }
                for (i = i; i < 18; i++){
                    printf(" ");
                }
                printf("| ");
            }
        
        printf("\n");

            for(r = 0; r < input_length; r++){
                for(c = 0; c < input_length - r; c++){
                    cyk_node* cur = NULL;
                    int i = 0;
                    cur = table[r][c];
                    while(cur != NULL){
                        int j = 0;
                        /* printf("%s %d,", cur->val, (int)(cur->probability * 100)); */
                        
                        if (cur->val){
                            const char * s = cur->val;
                            while( j < strlen(s) ){
                                printf("%c",s[j++]);
                                i++;
                                if ( j > 2)
                                    break;
                            }
                        printf(" ");
                        i++;
                        }

                        cur = cur->next;
                    }

                    for (i = i; i < 18; i++){
                        printf(" ");
                    }
                    printf("| ");
                }
                printf("\n");
            }
        }
        
        /* print output */
        s = table[input_length - 1][0];
        /*
        while(s != NULL){
            if(strcmp(s->val, "S") == 0){
                print_parse(s);
                printf(" %lf\n", s->probability);
            }
            s = s->next;
        }
        */

        if (1 == 1){
            /* grad portion */
            while(s != NULL){
                if(s->val && strcmp(s->val, "S") == 0){
                    if (best_prob < s->probability){
                        best_s = s;
                        best_prob = s->probability;
                    }
                }
                s = s->next;
            }
            

            if (best_s){
                print_parse(best_s);
                printf(" %lf\n", best_s->probability);
            }
        } else {
            /* undergrad */
            while(s != NULL){
                if(strcmp(s->val, "S") == 0){
                    print_parse(s);
                    printf(" %lf\n", s->probability);
                }
                s = s->next;
            } 
        }
        

        for(r = 0; r < input_length; r++){
            for(c = 0; c < input_length - r; c++){
                cyk_node* cur = table[r][c];
                while(cur != NULL){
                    cyk_node* temp = cur;
                    cur = cur->next;
                    free(temp);
                }
            }
            free(sentence[r]);
            free(table[r]);
        }
        free(sentence);
        free(table);

        exit(1);
    }

    void malloc_check(void* p){
        if(p == NULL)
        {
        fprintf(stderr, "Malloc failed\n");
        exit(1);
        }
    }
