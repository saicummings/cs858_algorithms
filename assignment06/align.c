/**
 * \file align.c
 *
 *
 *
 * \author jtd7
 * \date 03-10-2011
 */

#include "align_io.h"
#include "dfs_solver.h"
#include <sys/time.h>
#include <string.h>

typedef int(*compute_alignment)(char*, unsigned int, char*, unsigned int);

/* Gets the time of day in seconds. */
static double get_current_seconds(void){
    double sec, usec;
    struct timeval tv;

    if (gettimeofday(&tv, NULL) < 0) {
	perror("gettimeofday failed");
	exit(EXIT_FAILURE);
    }

    sec = tv.tv_sec;
    usec = tv.tv_usec;

    return sec + (usec / 1000000);
}


/*
 * Loads the tokens from the files and calls the output_diff() routine
 * to actually output the differences.
 */
static int align_files(char *f0, char *f1, compute_alignment c){
    int err = 0;
    char *ary0, *ary1;
    unsigned int n0, n1;
    double start, end;

    printf("Diffing %s and %s.\n", f0, f1);

    err = load_token_arrays(f0, f1, &ary0, &n0, &ary1, &n1);
    if (err) return 1;

    printf("%s contains %u characters.\n", f0, n0);
    printf("%s contains %u characters.\n", f1, n1);

    start = get_current_seconds();

    if (c != NULL) err = c(ary0,n0,ary1,n1);
    else err = 1;

    end = get_current_seconds();
    fprintf(stdout, "time: %f seconds\n", end - start);

    free(ary0);
    free(ary1);

    return err;
}

/*
 * Print the usage string and exit with failure status.
 */
static void usage(void){
    fprintf(stderr, "Usage\n" "align <alg> <file0> <file1>\n");
    exit(EXIT_FAILURE);
}


struct node {
	struct node *traceback;
	unsigned int score;
	char top;
	char left;
};

int dyn_programming_solution(char *string1, unsigned int len1,
			     char *string2, unsigned int len2){
	/* Here is the thing you have to fill in.*/
	int i, r, c;
	int max_len = len1 + len2 + 1;
	char * out1 = calloc(max_len, sizeof(char));
	char * out2 = calloc(max_len, sizeof(char));
	struct node * cur;
	struct node ** node_arr = malloc(sizeof(struct node*)*(len2 + 1));

	for(i = 0; i < len2 + 1; i++){
		node_arr[i] = malloc(sizeof(struct node)*(len1 + 1));
	}

	for(r = 0; r < len2 + 1; r++){
		for(c = 0; c < len1 + 1; c++){
			struct node * n = &node_arr[r][c];

			if(r == 0 && c == 0){
				/* upp left corner make null */
				n->score = 0;
				n->traceback = NULL;

			} else if (r == 0){
				/* top row have back trace to left */
				n->score = 0;
				n->traceback = &node_arr[r][c - 1];
				n->top = string1[c - 1];
				n->left = '-';
				
			} else if (c == 0){
				/* most left column back trace up */
				n->score = 0;
				n->traceback = &node_arr[r - 1][c];
				n->top = '-';
				n->left = string2[r - 1];

			} else {
				/* check chars */
				if (string2[r - 1] - string1[c - 1] == 0){
					/* char matches */
					n->score = node_arr[r - 1][c - 1].score + 1;
					n->traceback = &node_arr[r - 1][c - 1];
					n->top = string1[c - 1];
					n->left = string2[r - 1];
				} else {
					int top_score =  node_arr[r - 1][c].score;
					int left_score =  node_arr[r][c - 1].score;

					if (top_score > left_score){
						n->score = node_arr[r - 1][c].score;
						n->traceback = &node_arr[r - 1][c];
						n->top = '-';
						n->left = string2[r - 1];

					} else {
						n->score = node_arr[r][c - 1].score;
						n->traceback = &node_arr[r][c - 1];
						n->top = string1[c - 1];
						n->left = '-';
					}
				}
			}
		}
	}

	/* perform trace back */
	cur = &node_arr[len2][len1];
	printf("Alignment Score: %i\n", cur->score);

	
	i = max_len - 1;
	while(cur->traceback != NULL){
		--i;
		out1[i] = cur->top;
		out2[i] = cur->left;
		cur = cur->traceback;
	}

	output_alignment(out1 + i, out2 + i);
	

	free(out1);
	free(out2);
	for(i = 0; i < len2 + 1; i++){
		free(node_arr[i]);
	}
	free(node_arr);

	/* Find the alignment */
	/* print the max score */
	/* Display the alignment */


	return 0;
}


int main(int argc, char *const argv[]){
	char *alg, *f0, *f1;
	compute_alignment c = NULL;
	int err;

	if(argc < 3) usage();
	alg = argv[1];
	f0 = argv[2];
	f1 = argv[3];
	if(strcmp(alg,"dfs") == 0){
		c = &call_dfs_solver; /* from dfs_solver.c */
	}else if(strcmp(alg,"dyn") == 0){
		c = &dyn_programming_solution;
	}else{
		printf("Algorithm %s not recognized, expected dfs or dyn\n",
		       alg);
		return EXIT_FAILURE;
	}
	err = align_files(f0, f1,c);
	if (err) return EXIT_FAILURE;
	return EXIT_SUCCESS;
}
