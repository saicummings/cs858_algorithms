/**
 * \file bst.h
 *
 * A binary search rbtree on disk locations.
 *
 * \author eaburns, sai lekyang
 * \date 09-17-18
 */

#if !defined(_RBT_H_)
#define _RBT_H_

#include "disk_loc.h"

/* A simple binary rbtree node of disk locations. */
struct rbtree_node {
	struct disk_location loc;
	struct rbtree_node *left;
	struct rbtree_node *right;
	struct rbtree_node *parent;
	int isblack; /*-1 root sent, 0 red, 1 black, -2 nil node */ 
	int height;
};

/*
 * Inserts a location into the binary rbtree.
 *
 * Returns 0 on success and 1 on failure.
 */
int insert_rbtree_location(struct rbtree_node **root, struct disk_location *loc);

/* Frees the memory allocated for the location rbtree. */
void free_rbtree(struct rbtree_node *root);

/*
 * Removes the given node from the rbtree.
 *
 * Blame Cormen, Leiserson, Rivest and Stein for the awful variable
 * names.
 */
void remove_rbtree_node(struct rbtree_node **nodep, struct rbtree_node ** root);

/*
 * Remove the given location from the rbtree if it is there.
 *
 * Returns 1 if the location was removed and 0 if not (because it was
 * not found).
 */
int remove_rbtree_location(struct rbtree_node **root, struct disk_location *loc);

/*
 * Gets up to 'n' elements that come directly after (and including)
 * 'loc'.
 */
int rbtree_n_after(struct rbtree_node *node, struct disk_location *loc,
		 struct disk_location *locs[], unsigned int n,
		 unsigned int fill);

/*
 * Gets up to 'n' elements that come directly before (and including)
 * 'loc'.
 */
int rbtree_n_before(struct rbtree_node *node, struct disk_location *loc,
		  struct disk_location *locs[], unsigned int n,
		  unsigned int fill);

/*
 * Output the rbtree to the given file.
 */
void output_rbtree(FILE *outfile, int depth, struct rbtree_node *root);




#endif /* !_RBT_H_ */
