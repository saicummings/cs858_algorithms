/**
 * \file bst.c
 *
 * red black rbtree
 *
 * \author eaburns, sai lekyang
 * \date 09-17-18
 */

#include <assert.h>
#include <stdlib.h>

#include "rbt.h"
#include "disk_loc.h"

#define DEBUG 0

static struct rbtree_node * _root;

/* Mehtods for debugging */
int update_height(struct rbtree_node * root){
	int ht_l;
	int ht_r;

	if (root == NULL)
		return 0;

	if (root->isblack == -1 )
		root = root -> left;

	ht_l = update_height(root->left);
	ht_r = update_height(root->right);

	if (ht_l == 0 || ht_r == 0 ){
		if (ht_l > ht_r)
			root->height = ht_l + root->isblack;
		else 
			root->height = ht_r + root->isblack;
	} else if (ht_l != ht_r || ht_l == -1 || ht_r == -1){
		root->height = -1;
	}
	else {
		if (ht_l > ht_r)
			root->height = ht_l + root->isblack;
		else 
			root->height = ht_r + root->isblack;
	}
	
	return root->height;
}

/* Mehtods for debugging */
void print_rbt(struct rbtree_node * root, int depth)
{
	unsigned int i;

	if (root && root->isblack == -1 )
		root = root -> left;

	for (i = 0; i < depth; i += 1)
		printf("\t");
	if (root) {
		if (root->isblack == 1)
			printf("%d [%d %d] black",root->height, root->loc.track, root->loc.sector);
		else if (root->isblack == -1)
			printf("%d %d nil", root->loc.track, root->loc.sector);
		else
			printf("%d [%d %d] *red*",root->height, root->loc.track, root->loc.sector);

		printf("\n");
		print_rbt(root->left, depth + 1);
		print_rbt(root->right, depth + 1);
	} else {
		printf("(nil)\n");
	}
}

/*
 * Rotate left
 *
 */
static void rotate_left(struct rbtree_node **n){
	/*struct rbtree_node** root;*/
	struct rbtree_node* x = (*n);
	struct rbtree_node* y = x->right;

	if (DEBUG)
		printf("%s%d\n", "rotate_left on ", (*n)->loc.track);

	x->right = y->left;

	if (y->left){
		y->left->parent = x;
	}
	
	y->parent = x->parent;
	
	if (x->parent->isblack == -1){
		/* if x is root, y becomes root */
		x->parent->left = y;

	} else if (x == x->parent->left){
		x->parent->left = y;
	} else {
		x->parent->right = y;
	}
	y->left = x;
	x->parent = y;
}


/*
 * Rotate right
 *
 */
static void rotate_right(struct rbtree_node **n){
	struct rbtree_node* y = (*n);
	struct rbtree_node* x = y->left;

	if (DEBUG)
		printf("%s%d\n", "rotate_right on ", (*n)->loc.track);

	y->left = x->right;

	if (x->right){
		x->right->parent = y;
	}
	
	x->parent = y->parent;
	
	if (y->parent->isblack == -1){
		/* if y is root, x becomes root */
		y->parent->left = x;

	} else if (y == y->parent->left){
		y->parent->left = x;
	} else {
		y->parent->right = x;
	}
	x->right = y;
	y->parent = x;
}

/*
 * Allocates a new node in the binary rbtree and returns a pointer to it
 * via 'n'.
 *
 * Return 0 on success and 1 on failure.
 */
static int make_rbtree_node(struct disk_location *loc, struct rbtree_node **n,
	struct rbtree_node **parent)
{
	struct rbtree_node *node;
	struct rbtree_node *root;

	node = calloc(1, sizeof(*node));
	if (!node) {
		perror("calloc failed");
		return 1;
	}

	node->loc = *loc;

	if (DEBUG)
		printf("\n+++ ADDING %d\n\n", loc->track);

	if ( parent == NULL){
		root = calloc(1, sizeof(*root));
		if (!root) {
			perror("calloc failed");
			return 1;
		}

		_root = root;

		root->left = node;
		root->isblack = -1;
		*n = root;

		if (DEBUG)
			printf("%s\n", "no parent, this is the root");

		node->parent = root;
		node->isblack = 1;

	} else {
		node->parent = *parent;
		node->isblack = 0;
		if (DEBUG)
			printf("%s %d\n", "parent is ", node->parent->loc.track);
		*n = node;
	}

	
	/* Fix-insert */
	/* If parent is red, need to fix */
	while(node->parent->isblack != -1 && node->parent->isblack == 0){
		if (DEBUG)
			printf("%s\n", "FIX INSERT");

		if (node->parent == node->parent->parent->left){
			/* If parent is a left child */
			struct rbtree_node *uncle;

			if (DEBUG)
				printf("%s\n", "left child");

			uncle = node->parent->parent->right;

			if (uncle != NULL && uncle->isblack == 0){
				/* Case 1, r uncle is red */
				if (DEBUG)
					printf("%s\n", "case1");

				node->parent->isblack = 1;
				uncle->isblack = 1;
				node->parent->parent->isblack = 0;
				node = node->parent->parent;
			} else {
				 if (node == node->parent->right){
					/* Case 2, uncle is black and r is right child */
					
					if (DEBUG)
						printf("%s\n", "case2");
					node = node->parent;
					/* rotate left on r */ 
					rotate_left(&node);
				}
				node->parent->isblack = 1;
				node->parent->parent->isblack = 0;
				/* rotate right rotate right on grandparent */
				rotate_right(&node->parent->parent);
			}
		} else {
			/* If parent is a right child */
			struct rbtree_node *uncle;

			if (DEBUG)
				printf("%s\n", "right child");

			uncle = node->parent->parent->left;

			if (uncle != NULL && uncle->isblack == 0){
				/* Case 1, r uncle is red */

				if (DEBUG)
					printf("%s\n", "case1");

				node->parent->isblack = 1;
				uncle->isblack = 1;
				node->parent->parent->isblack = 0;
				node = node->parent->parent;
			} else {
				if (node == node->parent->left){
				/* Case 2, uncle is black and r is right child */

					if (DEBUG)
						printf("%s\n", "case2");

					node = node->parent;
					/* rotate right on r */ 
					rotate_right(&node);
				}
				node->parent->isblack = 1;
				node->parent->parent->isblack = 0;
				/* rotate left rotate right on grandparent */

				rotate_left(&node->parent->parent);
			}
		}
	}

	if (node->parent->isblack == -1)
		node->isblack = 1;

	if (DEBUG){
		update_height(_root);
		print_rbt(_root, 0);
	}

	return 0;
}

static int insert_rbtree_location_helper(struct rbtree_node **root, struct disk_location *loc,
	struct rbtree_node **parent)
{
	struct rbtree_node *r;
	assert (root);
	r = *root;

	if (!r){		
		return make_rbtree_node(loc, root, parent);
	}

	if (compare_locations(loc, &r->loc) <= 0)
		return  insert_rbtree_location_helper(&r->left, loc, root);

	return insert_rbtree_location_helper(&r->right, loc, root);
}

int insert_rbtree_location(struct rbtree_node **root, struct disk_location *loc)
{
	struct rbtree_node *r;
	assert (root);

	r = *root;

	if (!r){		
		return make_rbtree_node(loc, root, NULL);
	}

	r = r->left;

	if (compare_locations(loc, &r->loc) <= 0)
		return  insert_rbtree_location_helper(&r->left, loc, &r);

	return insert_rbtree_location_helper(&r->right, loc, &r);
}

void free_rbtree(struct rbtree_node *root)
{
	if (root) {
		free_rbtree(root->left);
		free_rbtree(root->right);
		free(root);
	}
}

/*
 * Returns the left-most child and its parent is returned via the
 * 'parent' argument
 */
static struct rbtree_node *minimum_with_parent(struct rbtree_node *node,
					     struct rbtree_node *prev,
					     struct rbtree_node **parent)
{
	if (!node)
		return NULL;

	if (node->isblack == -1 )
		node = node -> left;

	while (node->left) {
		prev = node;
		node = node->left;
	}

	*parent = prev;

	return node;
}


static void transpant(struct rbtree_node **uu, struct rbtree_node **vv){
	struct rbtree_node *u, *v;
	u = *uu;
	v = *vv;
	if (u->parent->isblack == -1)
		u->parent->left = v;
	else if (u == u->parent->left)
		u->parent->left = v;
	else 
		u->parent->right = v;
	v->parent = u->parent;
}


void remove_rbtree_node(struct rbtree_node **nodep, struct rbtree_node ** root)
{
	struct rbtree_node *z, *y, *py, *x, *nil;
	int org_color;

	nil = NULL;

	if (DEBUG){
		printf("--- REMOVE %d\n", (*nodep)->loc.track);
	}

	y = *nodep;
	z = *nodep;

	org_color = y->isblack;

	if (z->left == NULL){

		if(z->right){
			x = z->right;
		} else {

			x = calloc(1, sizeof(*x));
			if (!x) {
				perror("calloc failed");
			return;
			}

			x->isblack = 1;
			x->loc.track = -9999;
			x->parent = z;
			z->right = x;

			nil = x;
		}
		transpant(&z, &z->right);
	} else if (z->right == NULL){

		if(z->left){
			x = z->left;
		} else {

			x = calloc(1, sizeof(*x));
			if (!x) {
				perror("calloc failed");
			return;
			}
			x->isblack = 1;
			x->loc.track = -9999;
			x->parent = z;
			z->left = x;

			nil = x;
		}
		transpant(&z, &z->left);
	} else {
		
		/* 'minimum' is sufficient instead of 'successor'
		 * because this branch only happens if both the left
		 * and right children are non NULL. */
		y = minimum_with_parent(z->right, z, &py);
		org_color = y->isblack;

		if (DEBUG)
			printf("%s%d\n", "finding min y, ", y->loc.track);

		if(y->right){
			x = y->right;
		} else {

			x = calloc(1, sizeof(*x));
			if (!x) {
				perror("calloc failed");
			return;
			}
			x->isblack = 1;
			x->loc.track = -9999;
			x->parent = y;
			y->right = x;
			nil = x;
		}

		if (y->parent == z){
			x->parent = y;
		} else {
			transpant(&y, &y->right);
			y->right = z->right;
			y->right->parent = y;
		}
		transpant(&z, &y);
		y->left = z->left;
		y->left->parent = y;
		y->isblack = z->isblack;

		free(z);
	}

	if (org_color != 1){
		if(nil){
			if(nil->parent->left == nil){
				nil->parent->left = NULL;
			} else {
				nil->parent->right = NULL;
			}

			free(nil);
		}
		if (DEBUG){
			update_height(_root);
			print_rbt(_root, 0);
			printf("\n:: %s ::\n\n", "NO FIX NEEDED");
	
		}
		return;
	}



	/* Deletetion fix up on x */
	while(x->parent->isblack != -1 && x->isblack == 1){
		if (DEBUG){
			printf("\n:: %s ::\n\n", "FIX DELETE");
			printf("x is %d\n", x->loc.track);
		}

		if (x == x->parent->left){
			struct rbtree_node *w;
			int chld_flag = 0;

			if (DEBUG)
					printf("%s\n", "x is left child");

			w = x->parent->right;

			if (DEBUG)
				printf("w is %d\n", w->loc.track);

			if (w->isblack == 0){
				if (DEBUG)
					printf("%s\n", "case1, w is red");

				w->isblack = 1;
				x->parent->isblack = 0;
				rotate_left(&x->parent);
				w = x->parent->right;

				if(nil){
					if(nil->parent->left == nil){
						nil->parent->left = NULL;
					} else {
						nil->parent->right = NULL;
					}
				}

				if (DEBUG)
					printf("w is now %d\n", w->loc.track);
			}

			if (DEBUG){
				printf("checking w children: ");
				if (w->right == NULL){
					printf("%s ", "r null ");
				} else {
					printf("w right %d ", w->left->loc.track);
				}
				if (w->left == NULL){
					printf("%s ", "l null ");
				} else {
					printf("w left %d ", w->left->loc.track);
				}
				printf("%s\n", "");
			}

			if (!w->right && !w->left){
				chld_flag = 1;
			} else if (w->right != NULL && w->left != NULL){
				if (w->right->isblack == 1 && w->left->isblack == 1){
					chld_flag = 1;
				}
			}
			if (chld_flag == 1){
				w->isblack = 0;
				x = x->parent;
				if (DEBUG){
					printf("%s\n", "case 2, w children are black");
					printf("setting w %d to red\n", w->loc.track);
					printf("x is now parent, %d\n", x->loc.track);
				}
			} else {
				if (!w->right || w->right->isblack == 1){
					if (DEBUG)
						printf("%s\n", "case 3");
					w->left->isblack = 1;
					w->isblack = 0;
					rotate_right(&w);
					w = x->parent->right;

					if(nil){
						if(nil->parent->left == nil){
							nil->parent->left = NULL;
						} else {
							nil->parent->right = NULL;
						}
					}

				}
				if (DEBUG)
						printf("%s\n", "case 4");

				w->isblack = x->parent->isblack;
				x->parent->isblack = 1;
				w->right->isblack = 1;
				rotate_left(&x->parent);

				if(nil){
					if(nil->parent->left == nil){
						nil->parent->left = NULL;
					} else {
						nil->parent->right = NULL;
					}
				}

				x = (*root)->left;
				x->parent = *root;
			}	

			if (DEBUG)
				printf("%s\n", "fin left child");

		} else {

			struct rbtree_node *w;
			int chld_flag = 0;

			if (DEBUG)
				printf("%s\n", "x is right child");

			w = x->parent->left;
			
			if (DEBUG)
				printf("w is %d\n", w->loc.track);

			if (w->isblack == 0){
				if (DEBUG)
					printf("%s\n", "case1, w is red");

				w->isblack = 1;
				x->parent->isblack = 0;
				rotate_right(&x->parent);
				w = x->parent->left;

				if(nil){
					if(nil->parent->left == nil){
						nil->parent->left = NULL;
					} else {
						nil->parent->right = NULL;
					}
				}

				if (DEBUG)
					printf("w is now %d\n", w->loc.track);
			}
			
			if (DEBUG){
				printf("checking w children: ");
				if (w->right == NULL){
					printf("%s ", "r null ");
				} else {
					printf("w right %d ", w->left->loc.track);
				}
				if (w->left == NULL){
					printf("%s ", "l null ");
				} else {
					printf("w left %d ", w->left->loc.track);
				}
				printf("%s\n", "");
			}


			if (!w->right && !w->left){
				chld_flag = 1;
			} else if (w->right && w->left){
				if (w->right->isblack == 1 && w->left->isblack == 1){
					chld_flag = 1;
				}
			}

			if (chld_flag == 1){
				w->isblack = 0;
				x = x->parent;
				if (DEBUG){
					printf("%s\n", "case 2, w children are black");
					printf("setting w %d to red\n", w->loc.track);
					printf("x is now parent, %d\n", x->loc.track);
				}


				if(nil){
					if(nil->parent->left == nil){
						nil->parent->left = NULL;
					} else {
						nil->parent->right = NULL;
					}
				}

			} else {
				if (w->left->isblack == 1){
					if (DEBUG)
						printf("%s\n", "case 3");
					w->right->isblack = 1;
					w->isblack = 0;
					rotate_left(&w);
					w = x->parent->left;

					if(nil){
						if(nil->parent->left == nil){
							nil->parent->left = NULL;
						} else {
							nil->parent->right = NULL;
						}
					}
					
				}

				if (DEBUG)
						printf("%s\n", "case 4");

				w->isblack = x->parent->isblack;
				x->parent->isblack = 1;
				w->left->isblack = 1;
				rotate_right(&x->parent);

				if(nil){
					if(nil->parent->left == nil){
						nil->parent->left = NULL;
					} else {
						nil->parent->right = NULL;
					}
				}

				x = (*root)->left;
				x->parent = *root;
			}	

			if (DEBUG)
				printf("%s\n", "fin right child");
		}	
	}

	if (DEBUG){
		printf("x is %d\n", x->loc.track);
		printf("\n:: %s ::\n\n", "DONE FIX DELETE");
	}

	x->isblack = 1;

	if (DEBUG){
		printf("%s\n\n\n", "*****************");
		update_height(_root);
		print_rbt(_root, 0);
	}
}


static int remove_rbtree_location_helper(struct rbtree_node **root, 
	struct disk_location *loc, struct rbtree_node ** t_root)
{
	int c;
	struct rbtree_node * r;

	assert(root);
	r = *root;

	if (!r)
		return 0;

	if (r->isblack == -1 ){
		r = r -> left;
	}

	c = compare_locations(loc, &r->loc);
	if (c == 0) {
		remove_rbtree_node(root, t_root);
		return 1;
	} else if (c < 0) {
		return remove_rbtree_location_helper(&r->left, loc, t_root);
	}

	return remove_rbtree_location_helper(&r->right, loc, t_root);
}


int remove_rbtree_location(struct rbtree_node **root, struct disk_location *loc)
{
	int c;
	struct rbtree_node * r;
	struct rbtree_node * t_root;

	assert(root);
	r = *root;

	if (!r)
		return 0;

	if (r->isblack == -1 ){
		t_root = r;
		r = r -> left;
	}

	c = compare_locations(loc, &r->loc);
	if (c == 0) {
		remove_rbtree_node(root, &t_root);
		return 1;
	} else if (c < 0) {
		return remove_rbtree_location_helper(&r->left, loc, &t_root);
	}

	return remove_rbtree_location_helper(&r->right, loc, &t_root);
}


int rbtree_n_after(struct rbtree_node *node, struct disk_location *loc,
		 struct disk_location *locs[], unsigned int n,
		 unsigned int fill)
{
	int c;

	if (!node)
		return fill;

	if (node->isblack == -1 )
		node = node -> left;

	c = compare_locations(&node->loc, loc);
	if (c < 0) {
		return rbtree_n_after(node->right, loc, locs, n, fill);
	} else if (c >= 0) {
		int f = rbtree_n_after(node->left, loc, locs, n, fill);
		if (f < n) {
			locs[f] = &node->loc;
			f += 1;
			f = rbtree_n_after(node->right, loc, locs, n, f);
		}

		return f;
	}

	return fill;
}


int rbtree_n_before(struct rbtree_node *node, struct disk_location *loc,
		  struct disk_location *locs[], unsigned int n,
		  unsigned int fill)
{
	int c;

	if (!node)
		return fill;

	if (node->isblack == -1 )
		node = node -> left;

	c = compare_locations(&node->loc, loc);
	if (c > 0) {
		return rbtree_n_before(node->left, loc, locs, n, fill);
	} else if (c <= 0) {
		int f = rbtree_n_before(node->right, loc, locs, n, fill);
		if (f < n) {
			locs[f] = &node->loc;
			f += 1;
			f = rbtree_n_before(node->left, loc, locs, n, f);
		}

		return f;
	}

	return fill;
}


void output_rbtree(FILE *outfile, int depth, struct rbtree_node *root)
{
	unsigned int i;
	if (root && root->isblack == -1 )
		root = root -> left;

	if (DEBUG)
		printf("%s\n", "printing");

	for (i = 0; i < depth; i += 1)
		fprintf(outfile, " ");
	if (root) {

		if (root->isblack)
			fprintf(outfile, "%d %d black", root->loc.track, root->loc.sector);
		else
			fprintf(outfile, "%d %d red", root->loc.track, root->loc.sector);

		fprintf(outfile, "\n");
		output_rbtree(outfile, depth + 1, root->left);
		output_rbtree(outfile, depth + 1, root->right);
	} else {
		fprintf(outfile, "(nil)\n");
	}
}
